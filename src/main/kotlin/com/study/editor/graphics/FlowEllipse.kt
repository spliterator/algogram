package com.study.editor.graphics

import com.study.editor.controller.State
import javafx.geometry.Point2D
import javafx.scene.shape.Ellipse
import javafx.scene.text.Text
import javafx.scene.text.TextAlignment
import org.w3c.dom.Document
import org.w3c.dom.Element

class FlowEllipse(var point: Point2D, val rWidth: Double, val rHeight: Double, val label: String) : FlowShape() {

    init {
        this.translateX = point.x - rWidth
        this.translateY = point.y - rHeight

        val text = Text(label)
        text.textAlignment = TextAlignment.CENTER

        val ellipse = Ellipse(this.translateX, this.translateY, rWidth, rHeight)
        ellipse.fill = State.figureColor
        ellipse.stroke = State.figureColor
        ellipse.strokeWidth = 2.0

        setGlowableOnMouseEnter()
        this.children.addAll(ellipse, text)
    }

    override fun getXmlData(doc: Document): Element {
        val createElement = doc.createElement("Object") //flowblock пуст
        createElement.setAttribute("type", this.typeSelector)
        createElement.setAttribute("name", id)
        val w = translateX + rWidth
        createElement.setAttribute("X", w.toString())
        val y = translateY + rHeight
        createElement.setAttribute("Y", y.toString())
        createElement.setAttribute("width", rWidth.toString())
        createElement.setAttribute("height", rHeight.toString())
        createElement.appendChild(doc.createTextNode(label))

        return createElement
    }
}
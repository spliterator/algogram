package com.study.editor.graphics

import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import org.w3c.dom.Document
import org.w3c.dom.Element

class FlowCircle(x: Double, y: Double, val radius: Double) : FlowShape() {

    val circle: Circle

    init {
        translateX = x - radius
        translateY = y - radius

        circle = Circle(translateX, translateY, radius)
        circle.fill = Color.TRANSPARENT
        circle.stroke = Color.TRANSPARENT

        setGlowableOnMouseEnter()
        children.addAll(circle)
    }

    override fun getXmlData(doc: Document): Element {
        val createElement = doc.createElement("Object")
        createElement.setAttribute("type", this.typeSelector)
        createElement.setAttribute("name", id)
        val w = translateX + radius
        createElement.setAttribute("X", w.toString())
        val y = translateY + radius
        createElement.setAttribute("Y", y.toString())
        createElement.setAttribute("radius", radius.toString())

        return createElement
    }
}
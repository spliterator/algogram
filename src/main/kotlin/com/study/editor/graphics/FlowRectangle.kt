package com.study.editor.graphics

import com.study.editor.controller.State
import javafx.geometry.Point2D
import javafx.scene.shape.Rectangle
import org.w3c.dom.Document
import org.w3c.dom.Element

class FlowRectangle(point: Point2D, val rWidth: Double, val rHeight: Double, val label: String) : FlowShapeWText(label) {

    init {
        translateX = point.x - rWidth / 2
        translateY = point.y - rHeight / 2

        val rectangle = Rectangle(translateX, translateY, rWidth, rHeight)
        rectangle.fill = State.figureColor
        rectangle.stroke = State.figureColor
        rectangle.strokeWidth = 2.0

        setGlowableOnMouseEnter()

        children.addAll(rectangle, textView)
    }

    override fun getXmlData(doc: Document): Element {

        val createElement = doc.createElement("Object") //flowblock нужно заполнить собой
        createElement.setAttribute("type", this.typeSelector)
        createElement.setAttribute("name", id)
        val w = translateX + rWidth / 2
        createElement.setAttribute("X", w.toString())
        val y = translateY + rHeight / 2
        createElement.setAttribute("Y", y.toString())
        createElement.setAttribute("width", rWidth.toString())
        createElement.setAttribute("height", rHeight.toString())
        createElement.appendChild(doc.createTextNode(label))

        return createElement
    }
}
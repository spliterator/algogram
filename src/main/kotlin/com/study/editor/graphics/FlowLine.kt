package com.study.editor.graphics

import com.study.editor.controller.State
import javafx.scene.layout.AnchorPane
import javafx.scene.shape.Line
import tornadofx.*

open class FlowLine(var shape1: FlowShape, var shape2: FlowShape) : AnchorPane() {

    val line = Line()

    init {
        children.add(line)

        line.startXProperty().bind(shape1.translateXProperty().add(shape1.widthProperty().divide(2)))
        line.startYProperty().bind(shape1.translateYProperty().add(shape1.heightProperty().divide(2)))
        line.endXProperty().bind(shape2.translateXProperty().add(shape2.widthProperty().divide(2)))
        line.endYProperty().bind(shape2.translateYProperty().add(shape2.heightProperty().divide(2)))

        line.stroke = State.figureColor
        line.strokeWidth = 2.0
    }
}

class FlowLineToRhomb(shape1: FlowShape, shape2: FlowShape) : FlowLine(shape1, shape2) {

    init {
        line.endXProperty().bind(shape2.translateXProperty().add(3))
        line.endYProperty().bind(shape2.translateYProperty().add(shape2.heightProperty().divide(2)))

        id = State.generateId(this)
    }
}

class FlowLineFromRhomb(shape1: FlowShape, shape2: FlowShape) : FlowLine(shape1, shape2) {

    init {
        line.startXProperty().bind(shape1.translateXProperty().add(shape1.widthProperty()).minus(3))
        line.startYProperty().bind(shape1.translateYProperty().add(shape1.heightProperty().divide(2)))
    }
}
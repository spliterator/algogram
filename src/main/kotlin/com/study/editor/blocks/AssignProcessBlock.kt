package com.study.editor.blocks

import com.study.editor.graphics.FlowInsertableLine
import com.study.editor.graphics.FlowRectangle
import com.study.editor.graphics.FlowShape
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Point2D
import javafx.scene.Node

class AssignProcessBlock(point: Point2D, rWidth: Double, rHeight: Double, label: String) : FlowBlock {

    val flowRectangle = FlowRectangle(point, rWidth, rHeight, label)

    override fun getTopShape(): FlowShape = flowRectangle

    override fun getBottomShape(): FlowShape = flowRectangle

    override fun getInsertableLines(): List<FlowInsertableLine> = emptyList()

    override fun getItems(): ObservableList<Node> = FXCollections.observableArrayList()

    override fun getShapes(): ObservableList<FlowShape> {
        return FXCollections.observableArrayList()
    }
}
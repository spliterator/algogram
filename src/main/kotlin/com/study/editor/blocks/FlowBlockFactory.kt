package com.study.editor.blocks

import javafx.geometry.Point2D

class FlowBlockFactory {

    companion object {

        val width = 160.0
        val height = 50.0

        val rwidth = width * 1.3
        val rheight = height * 1.3

        fun getStartEndBlock(point: Point2D): FlowBlock = StartEndBlock(point, width, height)

        fun getIOBlock(point: Point2D): FlowBlock = IOBlock(point, width, height, "Ввод/Вывод")

        fun getAssignmentBlock(point: Point2D): FlowBlock = AssignProcessBlock(point, width, height, "x := 0")

        fun getProcessBlock(point: Point2D): FlowBlock = AssignProcessBlock(point, width, height, "func()")

        fun getBranchingBlock(point: Point2D): FlowBlock = BranchingBlock(point, rwidth, rheight, "x > 0")

        fun getCycleBlock(point: Point2D): FlowBlock = CycleBlock(point, rwidth, rheight, "x < 0?")
    }
}
package com.study.editor.blocks

import com.study.editor.graphics.*
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Point2D
import javafx.scene.Node

class BranchingBlock : FlowBlock {

    val flowRhomb: FlowRhomb

    val circleTopLeft: FlowCircle
    val circleTopRight: FlowCircle
    val circleBottomLeft: FlowCircle
    val circleBottomRight: FlowCircle
    val circleBottomCenter: FlowCircle

    val lineRhombToTl: FlowLineToRhomb
    val lineRhombToTr: FlowLineFromRhomb
    val lineTlBl: FlowInsertableLine
    val lineTrBr: FlowInsertableLine
    val lineBlBc: FlowLine
    val lineBrBc: FlowLine

    var list: ObservableList<Node> = FXCollections.observableArrayList()

    constructor(point: Point2D, rWidth: Double, rHeight: Double, label: String) {
        flowRhomb = FlowRhomb(point, rWidth, rHeight, label)
        val x = point.x
        val y = point.y
        val lineLength = 150
        val ypl = y + lineLength
        val xpl = x + lineLength
        val xml = x - lineLength
        val radius = 6.0
        circleTopLeft = FlowCircle(xml, y, radius)
        circleTopRight = FlowCircle(xpl, y, radius)
        circleBottomLeft = FlowCircle(xml, ypl, radius)
        circleBottomRight = FlowCircle(xpl, ypl, radius)
        circleBottomCenter = FlowCircle(x, ypl, radius)

        lineRhombToTl = FlowLineToRhomb(circleTopLeft, flowRhomb)
        lineRhombToTr = FlowLineFromRhomb(flowRhomb, circleTopRight)
        lineTlBl = FlowInsertableLine(circleTopLeft, circleBottomLeft, list)
        lineTrBr = FlowInsertableLine(circleTopRight, circleBottomRight, list)
        lineBlBc = FlowLine(circleBottomLeft, circleBottomCenter)
        lineBrBc = FlowLine(circleBottomRight, circleBottomCenter)

        list.addAll(listOf(circleTopLeft, circleTopRight, circleBottomLeft, circleBottomRight,
                circleBottomCenter, lineRhombToTl.line, lineRhombToTr.line, lineBlBc.line, lineBrBc.line) +
                lineTlBl.getElements() + lineTrBr.getElements())
    }

    constructor(point: Point2D, rWidth: Double, rHeight: Double, label: String, actl: FlowCircle, actr: FlowCircle, acbl: FlowCircle, acbr: FlowCircle, acbc: FlowCircle) {
        flowRhomb = FlowRhomb(point, rWidth, rHeight, label)
        val x = point.x
        val y = point.y
        val lineLength = 150
        val ypl = y + lineLength
        val xpl = x + lineLength
        val xml = x - lineLength
        val radius = 6.0
        circleTopLeft = actl
        circleTopRight = actr
        circleBottomLeft = acbl
        circleBottomRight = acbr
        circleBottomCenter = acbc

        lineRhombToTl = FlowLineToRhomb(circleTopLeft, flowRhomb)
        lineRhombToTr = FlowLineFromRhomb(flowRhomb, circleTopRight)
        lineTlBl = FlowInsertableLine(circleTopLeft, circleBottomLeft, list)
        lineTrBr = FlowInsertableLine(circleTopRight, circleBottomRight, list)
        lineBlBc = FlowLine(circleBottomLeft, circleBottomCenter)
        lineBrBc = FlowLine(circleBottomRight, circleBottomCenter)

        list.addAll(listOf(circleTopLeft, circleTopRight, circleBottomLeft, circleBottomRight,
                circleBottomCenter, lineRhombToTl.line, lineRhombToTr.line, lineBlBc.line, lineBrBc.line) +
                lineTlBl.getElements() + lineTrBr.getElements())

    }

    override fun getTopShape(): FlowShape = flowRhomb

    override fun getBottomShape(): FlowShape = circleBottomCenter

    override fun getInsertableLines(): List<FlowInsertableLine> = listOf(lineTlBl, lineTrBr)

    override fun getItems(): ObservableList<Node> = list

    override fun getShapes(): ObservableList<FlowShape> {
        return FXCollections.observableArrayList(circleTopLeft, circleTopRight, circleBottomLeft, circleBottomRight)
    }
}
package com.study.editor.service

import com.study.editor.blocks.AssignProcessBlock
import com.study.editor.blocks.BranchingBlock
import com.study.editor.blocks.CycleBlock
import com.study.editor.blocks.IOBlock
import com.study.editor.controller.State
import com.study.editor.graphics.FlowCircle
import com.study.editor.graphics.FlowEllipse
import com.study.editor.graphics.FlowInsertableLine
import com.study.editor.graphics.FlowShape
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Point2D
import javafx.scene.Node
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.NodeList
import java.io.File
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

object XmlService {

    fun createXmlFile(file: File) {
        val docBuilder: DocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        val doc: Document = docBuilder.newDocument()

        val rootElement: Element = doc.createElement("algogram")

        //добавим линии
        for (flowInsertableLine in State.insertLineList) {
            val newElement: Element = doc.createElement("InsertableLine")
            newElement.setAttribute("type", flowInsertableLine.typeSelector)
            newElement.setAttribute("name", flowInsertableLine.id)
            newElement.setAttribute("shape1", flowInsertableLine.shape1.id)
            newElement.setAttribute("shape2", flowInsertableLine.shape2.id)
            // для line нужны ток шейпы (уже есть), для circle аналогично
            rootElement.appendChild(newElement)
        }
        for (flowShape in State.shapeList) {
            rootElement.appendChild(flowShape.getXmlData(doc))
        }
        doc.appendChild(rootElement)

        val transformer: Transformer = TransformerFactory.newInstance().newTransformer()

        transformer.setOutputProperty(OutputKeys.INDENT, "yes")
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2")

        transformer.transform(DOMSource(doc), StreamResult(file))
    }

    fun openXmlFile(file: File) {
        val xmlFile = file
        val xmlDoc: Document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlFile)

        xmlDoc.documentElement.normalize()

        val parentList: ObservableList<Node> = FXCollections.observableArrayList()

        val objectNodeList: NodeList = xmlDoc.getElementsByTagName("Object")
        val objectMap: HashMap<String, Node> = HashMap()

        var isNeedSearchRhomb = false

        for (i in 0..objectNodeList.length - 1) {
            val item = objectNodeList.item(i)
            val itemName = item.attributes.getNamedItem("type")
            if (itemName.nodeValue.equals("FlowEllipse")) {
                val point: Point2D = Point2D(item.attributes.getNamedItem("X").nodeValue.toDouble(),
                        item.attributes.getNamedItem("Y").nodeValue.toDouble())
                val firstEllipse = FlowEllipse(point,
                        item.attributes.getNamedItem("width").nodeValue.toDouble(),
                        item.attributes.getNamedItem("height").nodeValue.toDouble(),
                        item.childNodes.item(0).nodeValue)
                firstEllipse.id = item.attributes.getNamedItem("name").nodeValue
                State.workSpace.add(firstEllipse)
                State.shapeList.add(firstEllipse)
                objectMap.put(firstEllipse.id, firstEllipse)
            }
            if (itemName.nodeValue.equals("FlowRectangle")) {
                val point: Point2D = Point2D(item.attributes.getNamedItem("X").nodeValue.toDouble(),
                        item.attributes.getNamedItem("Y").nodeValue.toDouble())
                val aRectangle = AssignProcessBlock(point,
                        item.attributes.getNamedItem("width").nodeValue.toDouble(),
                        item.attributes.getNamedItem("height").nodeValue.toDouble(),
                        item.childNodes.item(0).nodeValue)
                aRectangle.flowRectangle.id = item.attributes.getNamedItem("name").nodeValue
                State.workSpace.add(aRectangle.flowRectangle)
                State.shapeList.add(aRectangle.flowRectangle)
                objectMap.put(aRectangle.flowRectangle.id, aRectangle.flowRectangle)
                aRectangle.flowRectangle.flowBlock = aRectangle
            }
            if (itemName.nodeValue.equals("FlowParallelogram")) {
                val point: Point2D = Point2D(item.attributes.getNamedItem("X").nodeValue.toDouble(),
                        item.attributes.getNamedItem("Y").nodeValue.toDouble())
                val aParallelogram: IOBlock = IOBlock(point,
                        item.attributes.getNamedItem("width").nodeValue.toDouble(),
                        item.attributes.getNamedItem("height").nodeValue.toDouble(),
                        item.childNodes.item(0).nodeValue)
                aParallelogram.flowParallelogram.id = item.attributes.getNamedItem("name").nodeValue
                State.workSpace.add(aParallelogram.flowParallelogram)
                State.shapeList.add(aParallelogram.flowParallelogram)
                objectMap.put(aParallelogram.flowParallelogram.id, aParallelogram.flowParallelogram)
                aParallelogram.flowParallelogram.flowBlock = aParallelogram
            }
            if (itemName.nodeValue.equals("FlowBranchingRhomb")) {
                isNeedSearchRhomb = true
                val point: Point2D = Point2D(item.attributes.getNamedItem("X").nodeValue.toDouble(),
                        item.attributes.getNamedItem("Y").nodeValue.toDouble())

                var actl = FlowCircle(0.0, 0.0, 9.0)
                var actr = FlowCircle(0.0, 0.0, 9.0)
                var acbl = FlowCircle(0.0, 0.0, 9.0)
                var acbr = FlowCircle(0.0, 0.0, 9.0)
                var acbc = FlowCircle(0.0, 0.0, 9.0)
                for (i in 0..item.childNodes.length - 1) {
                    val item1 = item.childNodes.item(i)
                    if (item1.nodeName.equals("Object")) {
                        val point = Point2D(item1.attributes.getNamedItem("X").nodeValue.toDouble(),
                                item1.attributes.getNamedItem("Y").nodeValue.toDouble())
                        val flowCircle = FlowCircle(point.x,
                                point.y,
                                item1.attributes.getNamedItem("radius").nodeValue.toDouble())
                        flowCircle.id = item1.attributes.getNamedItem("name").nodeValue
                        val namedItem = item1.attributes.getNamedItem("type1").nodeValue
                        if (namedItem.equals("circleTopLeft")) {
                            actl = flowCircle
                            objectMap.put(flowCircle.id, actl)

                        } else if (namedItem.equals("circleTopRight")) {
                            actr = flowCircle
                            objectMap.put(flowCircle.id, actr)

                        } else if (namedItem.equals("circleBottomLeft")) {
                            acbl = flowCircle
                            objectMap.put(flowCircle.id, acbl)
                        } else if (namedItem.equals("circleBottomRight")) {
                            acbr = flowCircle
                            objectMap.put(flowCircle.id, acbr)

                        } else if (namedItem.equals("circleBottomCenter")) {
                            acbc = flowCircle
                            objectMap.put(flowCircle.id, acbc)
                            State.shapeList.addAll(acbc)
                            break
                        }
                    }
                }
                val aRhomb = BranchingBlock(point,
                        item.attributes.getNamedItem("width").nodeValue.toDouble(),
                        item.attributes.getNamedItem("height").nodeValue.toDouble(),
                        item.childNodes.item(0).nodeValue,
                        actl, actr, acbl, acbr, acbc)
                aRhomb.flowRhomb.id = item.attributes.getNamedItem("name").nodeValue
                State.workSpace.add(aRhomb.flowRhomb)
                State.shapeList.add(aRhomb.flowRhomb)
                objectMap.put(aRhomb.flowRhomb.id, aRhomb.flowRhomb)
                aRhomb.flowRhomb.flowBlock = aRhomb
                parentList.addAll(aRhomb.getItems())
                for (insertableLine in aRhomb.getInsertableLines()) {
                    parentList.remove(insertableLine.line)
                    parentList.remove(insertableLine.circle)
                }
                parentList.remove(aRhomb.circleBottomCenter)
                //добавим вспомогательные линии в workSpace

            }
            if (itemName.nodeValue.equals("FlowCycleRhomb")) {
                isNeedSearchRhomb = true
                val point: Point2D = Point2D(item.attributes.getNamedItem("X").nodeValue.toDouble(),
                        item.attributes.getNamedItem("Y").nodeValue.toDouble())

                var actr = FlowCircle(0.0, 0.0, 9.0)
                var actl = FlowCircle(0.0, 0.0, 9.0)
                var accl = FlowCircle(0.0, 0.0, 9.0)
                var acc = FlowCircle(0.0, 0.0, 9.0)
                var acct = FlowCircle(0.0, 0.0, 9.0)
                var acbr = FlowCircle(0.0, 0.0, 9.0)
                var acbc = FlowCircle(0.0, 0.0, 9.0)
                for (i in 0..item.childNodes.length - 1) {
                    val item1 = item.childNodes.item(i)
                    if (item1.nodeName.equals("Object")) {
                        val point = Point2D(item1.attributes.getNamedItem("X").nodeValue.toDouble(),
                                item1.attributes.getNamedItem("Y").nodeValue.toDouble())
                        val flowCircle = FlowCircle(point.x,
                                point.y,
                                item1.attributes.getNamedItem("radius").nodeValue.toDouble())
                        flowCircle.id = item1.attributes.getNamedItem("name").nodeValue
                        val namedItem = item1.attributes.getNamedItem("type1").nodeValue
                        if (namedItem.equals("circleTopRight")) {
                            actr = flowCircle
                            objectMap.put(flowCircle.id, actr)

                        } else if (namedItem.equals("circleBottomCenter")) {
                            acbc = flowCircle
                            objectMap.put(flowCircle.id, acbc)

                        } else if (namedItem.equals("circleBottomRight")) {
                            acbr = flowCircle
                            objectMap.put(flowCircle.id, acbr)
                        } else if (namedItem.equals("circleTopLeft")) {
                            actl = flowCircle
                            objectMap.put(flowCircle.id, actl)

                        } else if (namedItem.equals("circleCenterLeft")) {
                            accl = flowCircle
                            objectMap.put(flowCircle.id, accl)
                        } else if (namedItem.equals("circleCenter")) {
                            acc = flowCircle
                            objectMap.put(flowCircle.id, acc)

                        } else if (namedItem.equals("circleCenterTop")) {
                            acct = flowCircle
                            objectMap.put(flowCircle.id, acct)
                            State.shapeList.addAll(acbc)
                        }

                    }
                }
                val aRhomb = CycleBlock(point,
                        item.attributes.getNamedItem("width").nodeValue.toDouble(),
                        item.attributes.getNamedItem("height").nodeValue.toDouble(),
                        item.childNodes.item(0).nodeValue,
                        actr,
                        actl, accl, acc, acct, acbr, acbc)
                aRhomb.flowRhomb.id = item.attributes.getNamedItem("name").nodeValue
                State.workSpace.add(aRhomb.flowRhomb)
                State.shapeList.add(aRhomb.flowRhomb)
                objectMap.put(aRhomb.flowRhomb.id, aRhomb.flowRhomb)
                aRhomb.flowRhomb.flowBlock = aRhomb
                parentList.addAll(aRhomb.getItems())
                for (insertableLine in aRhomb.getInsertableLines()) {
                    parentList.remove(insertableLine.line)
                    parentList.remove(insertableLine.circle)
                }
                parentList.remove(aRhomb.circleBottomCenter)
            }
        }

        val lineNodeList: NodeList = xmlDoc.getElementsByTagName("InsertableLine")
        for (i in 0..lineNodeList.length - 1) {
            val item = lineNodeList.item(i)
            val shape1Name: String = item.attributes.getNamedItem("shape1").nodeValue
            val shape2Name: String = item.attributes.getNamedItem("shape2").nodeValue
            val lineName = item.attributes.getNamedItem("name").nodeValue

            val shape1: FlowShape = objectMap.get(shape1Name) as FlowShape
            val shape2: FlowShape = objectMap.get(shape2Name) as FlowShape

            val flowInsertableLine = FlowInsertableLine(shape1, shape2, parentList)
            flowInsertableLine.id = lineName
            var isLineWaFind = true
            if (isNeedSearchRhomb) {
                for (i in 0..State.insertLineList.size - 1) {
                    if (State.insertLineList.get(i).shape1.id.equals(shape1Name) && State.insertLineList.get(i).shape2.id.equals(shape2Name)) {
                        State.insertLineList.get(i).id = flowInsertableLine.id
                        isLineWaFind = false
                    }
                }
            }
            if (isLineWaFind) {

                shape1.line2 = flowInsertableLine
                shape2.line1 = flowInsertableLine
                State.workSpace.addAll(flowInsertableLine.getElements())
                State.insertLineList.add(flowInsertableLine)
            }
        }
        State.workSpace.clear()
        State.workSpace.addAll(parentList)
        for (flowInsertableLine in State.insertLineList) {
            State.workSpace.add(flowInsertableLine.line)
            State.workSpace.add(flowInsertableLine.circle)
        }
        State.workSpace.addAll(State.shapeList)
    }
}